# Table of Contents

1.  [Suckless Setup](#suckless-setup)
    1.  [Installation](#installation)
        1.  [Distribution/OS specific instructions](#os-instructions)
    2.  [Changes from the default configuration](#configuration-changes)
        1.  [dwm](#dwm-changes)
        2.  [st](#st-changes)
        3.  [dmenu](#dmenu-changes)
        4.  [slstatus](#slstatus-changes)
    3.  [Changes from the last update](#changes-from-last-update)
        1.  [dwm](#dwm-updates)
        2.  [st](#st-updates)
        3.  [dmenu](#dmenu-updates)
    4.  [Links](#links)


<a id="suckless-setup"></a>

# Suckless Setup

My personal suckless configuration. It includes code and installation notes for my personal builds of `dwm`, `st`, `dmenu` and `slstatus`.

![img](screenshot.png)


<a id="installation"></a>

## Installation

Go to the directory of the program you want to install and run `sudo make install` on your terminal.


<a id="os-instructions"></a>

### Distribution/OS specific instructions

-   Void Linux:
    -   Make sure you have the necessary devel packages installed (`base-devel`, `X11-devel`, `libXft-devel` and `libXinerama`)
    -   ~~Change the paths for the `X11INC` and `X11LIB` variables in `config.mk` to `/usr/include/X11` and `/usr/lib/X11` respectively before compiling.~~ You don't need to change the paths anymore.

-   FreeBSD / GhostBSD:
    -   Change the paths for the `X11INC` and `X11LIB` variables in `config.mk` to `$(PREFIX)/include` and `$(PREFIX)/lib` respectively before compiling.
    -   Uncomment the `FREETYPEINC` line for OpenBSD.
    -   Make sure you have `ncurses` installed on your system before installing `st`.
    -   Add the `-lkvm` flag in `LDFLAGS` before installing `slstatus`.


<a id="configuration-changes"></a>

## Changes from the default configuration


<a id="dwm-changes"></a>

### dwm

-   The default modifier key is set to `Mod4` (Super)
-   The `col_cyan` color variable is switched with another color variable (`col_gray5`)
-   The default font is set to Terminus 12
-   The `Mod+Shift+Enter` keybinding launches `st` by default
-   The following patches were applied:
    -   [alpha](https://dwm.suckless.org/patches/alpha/) (version 20230401) - Adds transparency on dwm (a compositor like `picom` is needed)
    -   [fullgaps](https://dwm.suckless.org/patches/fullgaps/) (version 20200508) - Adds gaps around and between windows
    -   [centretitle](https://dwm.suckless.org/patches/centretitle/) (version 20200907) - Aligns the window title on the center of the bar
    -   [titlecolor](https://dwm.suckless.org/patches/titlecolor/) (version 20210815) - Allows the color of the title to be set separately from the tabs color
    -   [alwayscenter](https://dwm.suckless.org/patches/alwayscenter/) (version 20200625) - Centers windows that spawn in floating mode


<a id="st-changes"></a>

### st

-   The default font is set to Terminus 16
-   The default TERM value is set to `xterm-256color`
-   The default colors are slightly changed (based on the `campbell` theme I used on [alacritty](https://github.com/eendroroy/alacritty-theme))
-   The following patches were applied:
    -   [alpha](https://st.suckless.org/patches/alpha/) (version 20220206) - Adds transparency on st (works similarly to the alpha patch for dwm)
    -   [anysize](https://st.suckless.org/patches/anysize/) (version 0.8.4) - Resizes st to fill the entire space
    -   [bold is not bright](https://st.suckless.org/patches/bold-is-not-bright/) (version 20190127) - Renders bold text without affecting its color


<a id="dmenu-changes"></a>

### dmenu

-   The default font and color configuration is set from dwm
-   A default prompt is set


<a id="slstatus-changes"></a>

### slstatus

-   Updates interval is set to `100ms`
-   The following status indicators were added:
    -   Wi-Fi percentage (versions for Linux and FreeBSD)
    -   Volume percentage (versions for OSS and PipeWire/PulseAudio)
    -   Keymap and keyboard indicators
    -   Date and time


<a id="changes-from-last-update"></a>

## Changes from the last update


<a id="dwm-updates"></a>

### dwm

-   Update to the latest version
-   Apply the patches
-   Minor tweaking of the configuration


<a id="st-updates"></a>

### st

-   Update to the latest version
-   Apply patches

<a id="dmenu-updates"></a>

### dmenu

-   Update to the latest version
-   Changed the default configuration


<a id="links"></a>

## Links

[Suckless.org](https://suckless.org)

[Suckless.org - Hacking](https://suckless.org/hacking)

[dwm](https://dwm.suckless.org)

[st](https://st.suckless.org)

[dmenu](https://tools.suckless.org/dmenu)

[slstatus](https://tools.suckless.org/slstatus)

